use std::env;
extern crate cc;
use cc::Build;

fn get_build() -> Build {
    let mut build = Build::new();
    build.compiler("clang");

    if let Ok(_) = build.try_get_compiler() {
        // We can't currently do cross-language LTO on stable, but try anyway.
        build
            .flag("-flto")
            .flag("-ffunction-sections");
        build
    } else {
        Build::new()
    }
}

fn main() {
    let target = env::var("TARGET").unwrap();

    if target.starts_with("thumb") && env::var_os("CARGO_FEATURE_INLINE_ASM").is_none() {
        let mut build = get_build();
        if target.starts_with("thumbv7") {
            build.define("ARM_V7M", "1");
        }

        if env::var_os("CARGO_FEATURE_CM7_R0P1").is_some() {
            build.define("ARM_V7M_R0P1", "1");
        }

        build
            .file("ops.c")
            .compile("ops");
    }

    if target.starts_with("thumbv6m-") {
        println!("cargo:rustc-cfg=cortex_m");
        println!("cargo:rustc-cfg=armv6m");
    } else if target.starts_with("thumbv7m-") {
        println!("cargo:rustc-cfg=cortex_m");
        println!("cargo:rustc-cfg=armv7m");
    } else if target.starts_with("thumbv7em-") {
        println!("cargo:rustc-cfg=cortex_m");
        println!("cargo:rustc-cfg=armv7m");
        //println!("cargo:rustc-cfg=armv7em");
    } else if target.starts_with("thumbv8m") {
        println!("cargo:rustc-cfg=cortex_m");
        println!("cargo:rustc-cfg=armv8m");
    }

    if target.ends_with("-eabihf") {
        println!("cargo:rustc-cfg=has_fpu");
    }
}
