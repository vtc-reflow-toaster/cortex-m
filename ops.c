#include <stdint.h>

#define INLINE __attribute__((always_inline))

void INLINE __bkpt() {
	asm volatile("bkpt" ::: "memory");
}

uint32_t INLINE __control() {
	uint32_t ret;
	asm volatile(
	"	mrs %0, CONTROL"
	: "=r"(ret));
	return ret;
}

void INLINE __cpsid() {
	asm volatile("cpsid i" ::: "memory");
}

void INLINE __cpsie() {
	asm volatile("cpsie i" ::: "memory");
}

void INLINE __delay(uint32_t n) {
	asm volatile(
	"1:	nop\n"
	"	subs %0, #1\n"
	"	bne 1b\n"
	: "+l"(n));
}

void INLINE __dmb() {
	asm volatile("dmb" ::: "memory");
}

void INLINE __dsb() {
	asm volatile("dsb" ::: "memory");
}

void INLINE __isb() {
	asm volatile("isb" ::: "memory");
}

uint32_t INLINE __msp_r() {
	uint32_t ret;
	asm volatile(
	"	mrs %0, MSP"
	: "=r"(ret));
	return ret;
}

void INLINE __msp_w(uint32_t val) {
	asm volatile(
	"	mrs MSP, %0"
	:
	: "r"(val));
}

void INLINE __nop() {
	asm volatile("nop");
}

uint32_t INLINE __primask() {
	uint32_t ret;
	asm volatile(
	"	mrs r0, PRIMASK"
	: "=r"(ret));
	return ret;
}

uint32_t INLINE __psp_r() {
	uint32_t ret;
	asm volatile(
	"	mrs %0, PSP"
	: "=r"(ret));
	return ret;
}

void INLINE __psp_w(uint32_t val) {
	asm volatile(
	"	msr PSP, %0"
	:
	: "r"(val));
}

void INLINE __sev() {
	asm volatile("sev");
}

void INLINE __wfe() {
	asm volatile("wfe" ::: "memory");
}

void INLINE __wfi() {
	asm volatile("wfi" ::: "memory");
}

#ifdef ARM_V7M
void INLINE __basepri_max(uint32_t val) {
	asm volatile(
	"	msr BASEPRI_MAX, %0"
	:
	: "r"(val));
}

uint32_t INLINE __basepri_r() {
	uint32_t ret;
	asm volatile(
	"	mrs %0, BASEPRI"
	: "=r"(ret));
	return ret;
}

void INLINE __basepri_w(uint32_t val) {
	asm volatile(
	"	msr BASEPRI, %0"
	:
	: "r"(val));
}

uint32_t INLINE __faultmask_max() {
	uint32_t ret;
	asm volatile(
	"	mrs %0, FAULTMASK_MAX"
	: "=r"(ret));
	return ret;
}

#ifdef ARM_V7M_R0P1
uint32_t __basepri_max_cm7_r0p1() {
	uint32_t pri = __primask();
	uint32_t ret;
	__cpsid();
	ret = __basepri_r();
	if (pri & 1)
		__cpsie();
	return ret;

}

void __basepri_w_cm7_r0p1(uint32_t val) {
	uint32_t pri = __primask();
	uint32_t ret;
	__cpsid();
	__basepri_w(val);
	if (pri & 1)
		__cpsie();
	return ret;
}
#endif // ARM_V7M_R0P1
#endif // ARM_V7M
